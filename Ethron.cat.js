//imports
const {cat} = require("ethron");
const babel = require("@ethronpi/babel");
const eslint = require("@ethronpi/eslint");
const exec = require("@ethronpi/exec");
const fs = require("@ethronpi/fs");
const npm = require("@ethronpi/npm");
const path = require("path");

//Package name.
const pkg = require("./package").name;

//User for publishing on NPM.
const who = "ethronpi";

//catalog
cat.macro("lint", [
  [exec, "dogmac check src test"],
  [eslint, "."]
]).title("Check source code");

cat.macro("trans-dogma", [
  [fs.rm, "build"],
  [exec, "dogmac js -o build/src src"],
  [exec, "dogmac js -o build/test test"],
]).title("Transpile from Dogma to JS");

cat.macro("trans-js", [
  [fs.rm, "dist"],
  [babel, "build", `dist/${pkg}/`]
]).title("Transpile from JS to JS");

cat.macro("build", [
  cat.get("lint"),
  cat.get("trans-dogma"),
  cat.get("trans-js"),
  [fs.cp, "package.json", `dist/${pkg}/package.json`],
  [fs.cp, "README.md", `dist/${pkg}/README.md`],
  [fs.cp, "test/data/config.yaml", path.join(process.env.HOME, ".verdaccio/conf/config.yaml")]
]).title("Build package");

cat.macro("test", `./dist/${pkg}/test/unit`).title("Run unit tests");

cat.call("pub", npm.publish, {
  who,
  access: "public",
  path: `dist/${pkg}`
}).title("Publish on NPM");

cat.macro("dflt", [
  cat.get("build"),
  cat.get("test")
]).title("Build and test");
