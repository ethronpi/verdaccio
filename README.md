# @ethronpi/verdaccio

[![NPM version](https://img.shields.io/npm/v/@ethronpi/verdaccio.svg)](https://npmjs.org/package/@ethronpi/verdaccio)
[![Total downloads](https://img.shields.io/npm/dt/@ethronpi/verdaccio.svg)](https://npmjs.org/package/@ethronpi/verdaccio)

[Ethron.js](http://ethronlabs.com) plugin for [Verdaccio](https://verdaccio.org/).

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Engineered in Valencia, Spain, EU by EthronLabs.*

## Use

```
const verdaccio = require("@ethronpi/verdaccio")
```

## verdaccio.run()

This task runs a Verdaccio Docker container:

```
verdaccio.run({image, container, volumes, publish})
```

- `sudo` (bool), use `sudo`. Default: `true`.
- `image` (string). Image to use. Default: `verdaccio/verdaccio`.
- `container` (string). Container name. Default: `verdaccio`.
- `publish` (string or number). Publish the port 4873 in the given port. Default: `4873`.
- `home` (string). Local Verdaccio home. Default: `~/.verdaccio`.
  This directory must contain: `conf/verdaccio.yaml`, `storage/` and `plugins/`.
- `rm` (bool). Remove container when stopped?

## verdaccio.start()

This task starts the Verdaccio Docker container:

```
verdaccio.start({container})
```

- `sudo` (bool), use `sudo`. Default: `true`.
- `container` (string). Container name. Default: `verdaccio`.

## verdaccio.stop()

This task stops the Verdaccio Docker container:

```
verdaccio.stop({container})
```

- `sudo` (bool), use `sudo`. Default: `true`.
- `container` (string). Container name. Default: `verdaccio`.
