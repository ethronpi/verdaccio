"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const assert = _core.dogma.use(require("@ethronjs/assert"));

const pkg = _core.dogma.use(require("../../../../@ethronpi/verdaccio"));

module.exports = exports = (0, _ethron.test)(__filename, () => {
  {
    assert(pkg).mem("run").isCallable().mem("start").isCallable().mem("stop").isCallable;
  }
});