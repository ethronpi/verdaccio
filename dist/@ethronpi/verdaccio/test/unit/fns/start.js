"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const path = _core.dogma.use(require("@dogmalang/path"));

const assert = _core.dogma.use(require("@ethronjs/assert"));

const HttpMessenger = _core.dogma.use(require("http-messenger"));

const verdaccio = _core.dogma.use(require("../../../../../@ethronpi/verdaccio"));

const start = _core.dogma.use(require("../../../../../@ethronpi/verdaccio/src/fns/start"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    const http = HttpMessenger({
      ["domain"]: "http://localhost:4873"
    });
    (0, _ethron.suite)("task", () => {
      {
        (0, _ethron.test)("fmt()", async () => {
          {
            assert(verdaccio.start._wrapped._fmt({
              ["container"]: "verdaccio"
            })).eq("Verdaccio: start container 'verdaccio'");
          }
        });
      }
    });
    (0, _ethron.suite)("fn", () => {
      {
        (0, _ethron.init)("run-verdaccio");
        (0, _ethron.init)("stop-verdaccio");
        (0, _ethron.fin)("rm-verdaccio");
        (0, _ethron.test)("using default", async () => {
          {
            0, await start({});
            0, await (0, _core.sleep)("1s");
            assert((0, await http.endpoint("/").get()).status()).eq(200);
          }
        }).fin("stop-verdaccio");
        (0, _ethron.test)("using explicit params", async () => {
          {
            0, await start({
              ["sleep"]: "1s"
            });
            assert((0, await http.endpoint("/").get()).status()).eq(200);
          }
        }).fin("stop-verdaccio");
      }
    });
  }
});