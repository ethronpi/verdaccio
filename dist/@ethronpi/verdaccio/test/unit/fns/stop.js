"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const path = _core.dogma.use(require("@dogmalang/path"));

const assert = _core.dogma.use(require("@ethronjs/assert"));

const HttpMessenger = _core.dogma.use(require("http-messenger"));

const verdaccio = _core.dogma.use(require("../../../../../@ethronpi/verdaccio"));

const stop = _core.dogma.use(require("../../../../../@ethronpi/verdaccio/src/fns/stop"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    const http = HttpMessenger({
      ["domain"]: "http://localhost:4873"
    });
    (0, _ethron.suite)("task", () => {
      {
        (0, _ethron.test)("fmt()", async () => {
          {
            assert(verdaccio.stop._wrapped._fmt({
              ["container"]: "verdaccio"
            })).eq("Verdaccio: stop container 'verdaccio'");
          }
        });
      }
    });
    (0, _ethron.suite)("fn", () => {
      {
        (0, _ethron.init)("run-verdaccio");
        (0, _ethron.fin)("rm-verdaccio");
        (0, _ethron.test)("using default", async () => {
          {
            0, await stop({});
            assert((await _core.dogma.pawait(() => http.endpoint("/").get()))).item(0).eq(false).item(1).like("ECONNREFUSED");
          }
        });
        (0, _ethron.test)("using explicit params", async () => {
          {
            0, await stop({});
            assert((await _core.dogma.pawait(() => http.endpoint("/").get()))).item(0).eq(false).item(1).like("ECONNREFUSED");
          }
        }).init("start-verdaccio");
      }
    });
  }
});