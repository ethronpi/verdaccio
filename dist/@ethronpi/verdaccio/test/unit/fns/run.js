"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const path = _core.dogma.use(require("@dogmalang/path"));

const assert = _core.dogma.use(require("@ethronjs/assert"));

const HttpMessenger = _core.dogma.use(require("http-messenger"));

const verdaccio = _core.dogma.use(require("../../../../../@ethronpi/verdaccio"));

const run = _core.dogma.use(require("../../../../../@ethronpi/verdaccio/src/fns/run"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    const http = HttpMessenger({
      ["domain"]: "http://localhost:4873"
    });
    (0, _ethron.suite)("task", () => {
      {
        (0, _ethron.test)("fmt()", async () => {
          {
            assert(verdaccio.run._wrapped._fmt({
              ["image"]: "verdaccio/verdaccio",
              ["container"]: "verdaccio"
            })).eq("Verdaccio: run container 'verdaccio' (verdaccio/verdaccio)");
          }
        });
      }
    });
    (0, _ethron.suite)("fn", () => {
      {
        (0, _ethron.suite)("error", () => {
          {
            (0, _ethron.test)("invalid publish", async () => {
              {
                assert((await _core.dogma.pawait(() => run({
                  ["publish"]: "4873a"
                })))).item(0).eq(false).item(1).eq("Invalid publish: 4873a.");
              }
            });
          }
        });
        (0, _ethron.test)("using default", async () => {
          {
            0, await run({});
            0, await (0, _core.sleep)("1s");
            assert((0, await http.endpoint("/").get()).status()).eq(200);
          }
        }).fin("stop-verdaccio").fin("rm-verdaccio");
        (0, _ethron.test)("using explicit params", async () => {
          {
            0, await run({
              ["sleep"]: "1s",
              ["rm"]: true
            });
            assert((0, await http.endpoint("/").get()).status()).eq(200);
          }
        }).fin("stop-verdaccio");
      }
    });
  }
});