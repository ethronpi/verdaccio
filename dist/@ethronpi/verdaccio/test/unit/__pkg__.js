"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _exec = require("@dogmalang/exec.async");

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.repo)("run-verdaccio", async () => {
      {
        0, await (0, _exec.exec)("sudo docker run --name verdaccio --detach --publish 4873:4873 " + "-v /home/me/.verdaccio/conf:/verdaccio/conf " + "-v /home/me/.verdaccio/storage:/verdaccio/storage " + "-v /home/me/.verdaccio/plugins:/verdaccio/plugins " + "verdaccio/verdaccio");
      }
    }).title("Run container");
    (0, _ethron.repo)("start-verdaccio", async () => {
      {
        0, await (0, _exec.exec)("sudo docker start verdaccio");
        0, await (0, _core.sleep)("1s");
      }
    }).title("Start container");
    (0, _ethron.repo)("stop-verdaccio", async () => {
      {
        0, await (0, _exec.exec)("sudo docker stop verdaccio");
      }
    }).title("Stop container");
    (0, _ethron.repo)("rm-verdaccio", async () => {
      {
        0, await (0, _exec.exec)("sudo docker rm verdaccio");
      }
    }).title("Remove container");
  }
});