"use strict";

var _core = require("@dogmalang/core");

var _exec = require("@dogmalang/exec.async");

const path = _core.dogma.use(require("@dogmalang/path"));

async function run(params) {
  /* istanbul ignore next */
  _core.dogma.paramExpectedToHave("params", params, {
    image: {
      type: _core.text,
      mandatory: false
    },
    container: {
      type: _core.text,
      mandatory: false
    },
    sudo: {
      type: _core.bool,
      mandatory: false
    },
    publish: {
      type: [_core.text, _core.num],
      mandatory: false
    },
    home: {
      type: _core.text,
      mandatory: false
    },
    rm: {
      type: _core.bool,
      mandatory: false
    }
  }, true);

  let {
    image,
    container,
    sudo,
    publish,
    home,
    rm
  } = params;
  {
    image = (0, _core.coalesce)(image, "verdaccio/verdaccio");
    container = (0, _core.coalesce)(container, "verdaccio");
    sudo = (0, _core.coalesce)(sudo, true);
    publish = (0, _core.coalesce)(publish, "4873");
    home = (0, _core.coalesce)(home, path.join(_core.ps.env.HOME, ".verdaccio"));
    rm = (0, _core.coalesce)(rm, false);

    if (_core.dogma.like(publish, "^[0-9]+$")) {
      publish = `${publish}:${publish}`;
    } else {
      _core.dogma.raise("Invalid publish: %s.", publish);
    }
    /*istanbul ignore next*/


    const cmd = (0, _core.fmt)("%s docker run --name %s --detach --publish %s -v %s/conf:/verdaccio/conf -v %s/storage:/verdaccio/storage -v %s/plugins:/verdaccio/plugins %s %s", sudo ? "sudo" : "", container, publish, home, home, home, rm ? "--rm" : "", image);
    0, await (0, _exec.exec)(cmd, {
      ["detache"]: true
    });

    if (params.sleep) {
      0, await (0, _core.sleep)(params.sleep);
    }
  }
}

module.exports = exports = run;