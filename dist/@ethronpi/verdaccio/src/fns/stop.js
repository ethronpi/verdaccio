"use strict";

var _core = require("@dogmalang/core");

var _exec = require("@dogmalang/exec.async");

async function stop(params) {
  /* istanbul ignore next */
  _core.dogma.paramExpectedToHave("params", params, {
    container: {
      type: _core.text,
      mandatory: false
    },
    sudo: {
      type: _core.bool,
      mandatory: false
    }
  }, true);

  let {
    container,
    sudo
  } = params;
  {
    container = (0, _core.coalesce)(container, "verdaccio");
    sudo = (0, _core.coalesce)(sudo, true);
    /*istanbul ignore next*/

    0, await (0, _exec.exec)((sudo ? "sudo " : "") + "docker stop " + container);
  }
}

module.exports = exports = stop;