"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.stop = exports.start = exports.run = void 0;

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const run = (0, _ethron.simple)({
  ["id"]: "run",
  ["desc"]: "Run Verdaccio Docker container.",
  ["fmt"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, _core.map);

    let {
      image,
      container
    } = params;
    {
      return `Verdaccio: run container '${container}' (${image})`;
    }
  }
}, _core.dogma.use(require("./fns/run")));
exports.run = run;
const start = (0, _ethron.simple)({
  ["id"]: "start",
  ["desc"]: "Start Verdaccio Docker container.",
  ["fmt"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, _core.map);

    let {
      image,
      container
    } = params;
    {
      return `Verdaccio: start container '${container}'`;
    }
  }
}, _core.dogma.use(require("./fns/start")));
exports.start = start;
const stop = (0, _ethron.simple)({
  ["id"]: "stop",
  ["desc"]: "Stop Verdaccio Docker container.",
  ["fmt"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, _core.map);

    let {
      image,
      container
    } = params;
    {
      return `Verdaccio: stop container '${container}'`;
    }
  }
}, _core.dogma.use(require("./fns/stop")));
exports.stop = stop;